/**
 * Created by suparngupta on 1/16/16.
 */
var gulp = require("gulp");
var concat = require("gulp-concat");
var conf = require("./conf");
var ngAnnotate = require("gulp-ng-annotate");
var uglify = require("gulp-uglify");
var less = require("gulp-less");
var del = require("del");
var templates = require("gulp-angular-templatecache");
var plumber = require("gulp-plumber");


gulp.task("vendorJS", function () {
    return gulp.src(conf.paths.vendor.js.src)
        .pipe(plumber())
        .pipe(concat('weecare.vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(conf.paths.vendor.js.dest));
});

gulp.task("nano", function(){
   return gulp.src("client/vendor/nano/dist/**")
       .pipe(gulp.dest(conf.paths.vendor.js.dest));
});

gulp.task("vendorCSS", function () {
    return gulp.src(conf.paths.vendor.css.src)
        .pipe(plumber())
        .pipe(concat('weecare.vendor.css'))
        .pipe(gulp.dest(conf.paths.vendor.css.dest));
});

gulp.task("clean", function () {
    return del(conf.paths.dist)
});

gulp.task("scripts", function () {
    return gulp.src(conf.paths.js.src)
        .pipe(concat('weecare.bundle.js'))
        .pipe(plumber())
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest(conf.paths.js.dest));
});

gulp.task("styles", function () {
    return gulp.src(conf.paths.css.main)
        .pipe(plumber())
        .pipe(less())
        .pipe(gulp.dest(conf.paths.css.dest));
});

gulp.task("templates", function () {
    return gulp.src(conf.paths.templates.src)
        .pipe(plumber())
        .pipe(templates({
            module: conf.module.name
        }))
        .pipe(gulp.dest(conf.paths.templates.dest));
});

gulp.task("images", function () {
    return gulp.src(conf.paths.images.src)
        .pipe(plumber())
        .pipe(gulp.dest(conf.paths.images.dest));
});

gulp.task("fonts", function () {
    return gulp.src(conf.paths.fonts.src)
        .pipe(plumber())
        .pipe(gulp.dest(conf.paths.fonts.dest));
});

gulp.task("build", ["vendorJS", "nano", "vendorCSS", "scripts", "styles", "templates", "images", "fonts"]);

gulp.task("dist", ["clean", "build"]);