/**
 * Created by suparngupta on 1/16/16.
 */
module.exports = {
    module: {
        name: "weecare"
    },
    paths: {
        dist: "./public/dist/**",
        vendor: {
            js: {
                src: ["client/vendor/jquery/dist/jquery.min.js",
                    "client/vendor/angular/angular.min.js",
                    "client/vendor/angular-ui-router/release/angular-ui-router.min.js",
                    "client/vendor/bootstrap/dist/js/bootstrap.min.js",
                    "client/vendor/unslider/dist/js/unslider-min.js"],
                dest: "./public/dist/js/"
            },
            css: {
                src: ["client/vendor/bootstrap/dist/css/bootstrap.min.css",
                    "client/vendor/font-awesome/css/font-awesome.min.css",
                    "client/vendor/theme/css/*.css",
                    "client/vendor/unslider/dist/css/unslider.css",
                    "client/vendor/unslider/dist/css/unslider-dots.css"],
                dest: "./public/dist/css"
            }
        },
        js: {
            src: ["./client/js/app.js", "./client/js/**/*.js"],
            dest: "./public/dist/js/"
        },
        css: {
            src: ["./client/less/*.less"],
            main: ["./client/less/main.less"],
            dest: "./public/dist/css"
        },
        templates: {
            src: ["./client/templates/**/*.html"],
            dest: "./public/dist/templates"
        },
        images: {
            src: ["./client/images/*"],
            dest: "./public/dist/images"
        },
        fonts: {
            src: ["client/vendor/font-awesome/fonts/*", "client/vendor/bootstrap/dist/fonts/*", "client/vendor/theme/fonts/*"],
            dest: "./public/dist/fonts"
        }
    }
};