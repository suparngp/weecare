/**
 * Created by suparngupta on 1/16/16.
 */

var gulp = require("gulp");
var watch = require("gulp-watch");
var conf = require("./conf");

gulp.task('watch', function () {
    gulp.watch(conf.paths.css.src, ["styles"]);
    gulp.watch(conf.paths.js.src, ["scripts"]);
    gulp.watch(conf.paths.templates.src, ["templates"]);
});