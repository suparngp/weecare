/**
 * Created by suparngupta on 1/17/16.
 */
var app = angular.module("weecare");

app.directive("footerBar", function (template) {
    return {
        restrict: "E",
        scope: {},
        templateUrl: template.fetchPartialWithName("footer-bar")
    }
});