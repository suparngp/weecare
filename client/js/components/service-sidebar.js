/**
 * Created by suparngupta on 1/17/16.
 */
angular.module("weecare")
.directive("serviceSidebar", function (template) {
    return {
        replace: true,
        restrict: "E",
        templateUrl: template.fetchPartialWithName("service-sidebar"),
        controller: "Sidebar"
    };
});