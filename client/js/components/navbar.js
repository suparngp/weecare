/**
 * Created by suparngupta on 1/17/16.
 */

var app = angular.module("weecare");

app.directive("navBar", function (template) {
    return {
        restrict: "E",
        templateUrl: template.fetchPartialWithName("nav-bar")
    }
});