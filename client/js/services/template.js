/**
 * Created by suparngupta on 1/17/16.
 */



angular.module("weecare")
.provider("template", function () {
    var fetchPartialWithName = function (name) {
        return "partials/" + name + ".html";
    };
    var fetchPageWithName = function (name) {
        return name + ".html";
    };
    return {
        fetchPageWithName: fetchPageWithName,
        fetchPartialWithName: fetchPartialWithName,
        $get: function () {
            return {
                fetchPartialWithName: fetchPartialWithName,
                fetchPageWithName: fetchPageWithName
            }
        }
    };
});

