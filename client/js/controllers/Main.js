/**
 * Created by suparngupta on 1/16/16.
 */

var app = angular.module("weecare");

app.controller("Main", function ($scope, $location, $anchorScroll, $state) {
    $scope.currentState = $state.current;
    $scope.scrollTo = function(id){
        console.log("I am here");
        $location.hash(id);
        $anchorScroll();
    };
    $scope.state = $state;
    console.log($state);
});