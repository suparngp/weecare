/**
 * Created by suparngupta on 1/17/16.
 */

angular.module("weecare")
    .controller("About", function ($scope) {

        $scope.data = [];


        $scope.speciality = {
            header: "Speciality",
            lead: "Child and New born Specialist \"M.D. Pediatrics\"",
            img: "/dist/images/shot.jpg",
            points: [
                "New born Care",
                "Preventive Pediatrics (Vaccinations, Growth & Development)",
                "Allergic conditions and Pediatric Asthma",
                "Vast experience in handling pre mature and new born babies"
            ]
        };

        $scope.qualifications = {
            header: "Experience and Qualifications",
            lead: "M.D. from MAMC, New Delhi- one of the finest medical institutes in India",
            points: ["Senior Resident - The Department of Paediatrics, Maulana Azad Medical College And associated  Lok Nayak Hospital, New Delhi",
                "M.D. Paediatrics - The Department of Paediatrics, Maulana Azad Medical College And associated  Lok Nayak Hospital, New Delhi",
                "Junior Resident in Burns & Plastic Surgery - University College of Medical Sciences, New Delhi",
                "Junior Resident in Neurosurgery - University College of Medical Sciences, New Delhi",
                "M.B.B.S - University College of Medical Sciences and associated GTB hospital, New Delhi"
            ]
        };

        $scope.memberships = {
            header: "Professional Memberships",
            points: [
                " Medical Council of India - Registration No. 19882",
                "Delhi Medical Council - Registration no. 602",
                "Indian Academy of Paediatrics Delhi Chapter - Registration no. DB/L/03/G to 118"
            ]
        };

        $scope.publications = {
            header: "Publications and Conferences",
            lead: "",
            points: [
                "Clinical features and antimicrobial susceptibility of Vibrio Cholerae (INABA) in children in Delhi. K Rajeshwari, Ashish Gupta," +
                " A P Dubey, Beena Uppal. Abstract published in the 43rd National Conference of Indian Academy of Paediatrics 2006, Pg 75",
                "Congenital Adrenal Hyperplasia presented at Delhi (Central Zone) Monthly IAP meet, MAMC",
                "Beta Sarcoglycanopathy. Kapoor S, Tatke M, Aggarwal S, Gupta A. . Indian J Paediatr, 2005, 72(1), 71 to 74.",
                "Clinical profile of neonatal seizures and their correlation with EEG in a referral unit. Ajay Kumar, " +
                "Ashish Gupta, N.B. Mathur, B.Talukdar. Paper presented at XXIII Annual convention of  National Neonatology Forum, Hyderabad",
                "Clinico Etiological study of seizures in inborn neonates and their correlation with EEG. Ashish Gupta, B.Talukdar, Ajay Kumar. " +
                "Paper presented at 42nd National Conference of the Indian Academy of Paediatrics (PEDICON, 2005), Kolkata",
                "Rajeshwari K, Gupta A, Uppal B, Dubey A P, Singh MM. Diarrheal outbreak of vibrio cholerae 01 inaba in Delhi. TROPICAL DOCTOR 2008;38: 105 To 107",
                "First national conference of Neurogenetics at MAMC, New Delhi",
                "Autism Training for Delhi Government Doctors at Maulana Azad Medical College, New Delhi",

            ]
        };

        $scope.data = [$scope.speciality, $scope.qualifications, $scope.memberships, $scope.publications];
    });