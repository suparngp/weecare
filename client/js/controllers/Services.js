/**
 * Created by suparngupta on 1/19/16.
 */
angular.module("weecare")
    .controller("Services", function ($scope) {
        $scope.highlightServices = [];
        $scope.otherServices = [
            {
                header: "Vaccination and Immunization",
                icon: "icon-i-immunizations",
                details: "We provide immunization services according to\
                international and national guidelines for best medical\
                practices and utmost care. Ample time is provided to\
                explain about vaccines, alternatives and their effects\
                and side effects. We provide most routine childhood\
                immunizations and follow the guidelines issued by the\
                Advisory Committee on Immunization Practices (AAP) and\
                Indian Academy of Paediatrics.  If you are unsure which\
                 vaccines your child needs, please see our recommended\
                 immunization schedule"
            },
            {
                header: "Pediatric Care & Consultation",
                icon: "icon-i-registration",
                details: "We take pride in ourselves while providing\
                compassionate care to children from birth to 18 years\
                of age. We give our expert medical opinion and treatment\
                 for various childhood medical problems. We offer round\
                 the clock help and form comprehensive plans for holistic\
                 management."
            },
            {
                header: "Respiratory Disorders",
                icon: "icon-i-respiratory",
                details: "Dr. Ashish Gupta is treating children for all childhood\
                problems but with a particular interest in the treatment of allergic\
                problems, breathing problems, wheezing, Asthma, pneumonia, etc."
            },
            {
                header: "Nutrition",
                icon: "icon-i-nutrition",
                details: "At every visit special care is given regarding nutrition\
                of child and then particular diet is advised depending on requirement.\
                We encourage parents to discuss diet and weight during their visit,\
                especially as there is an epidemic of obesity."
            }, {
                header: "New Born Care",
                icon: "icon-i-pediatrics",
                details: "Dr. Ashish Gupta attends term and premature deliveries " +
                "and caring for them in NICU( Neonatal Intensive Care Unit) if " +
                "needed. He later follows newborn on the outpatient basis and " +
                "gives them holistic care with exceptional attention to their " +
                "growth, development, feeding, and immunization.He will come to " +
                "the hospital to examine your baby within 24 hours of their birth" +
                " and then daily during your hospital stay. After discharge from " +
                "the hospital, we will want to see your newborn in our clinic " +
                "within a few days. *"
            },
            {
                header: "Growth & Development",
                icon: "icon-i-alternative-complementary",
                details: "We completely understand the need for healthy growth of your " +
                "little one. All children are regularly monitored for weight, height, " +
                "head circumference and by other parameters for growth and advised accordingly. " +
                "At regular intervals, child's development is assessed, and further decisions" +
                " are made on a child by child basis."
            }, {
                header: "Intensive / In-bed care",
                icon: "icon-i-inpatient",
                details: "In a case of need for admission, we admit the patient at Primus Superspeciality Hospital," +
                " Rockland Hospital, and  Saket City hospital. We treat children with " +
                "love and compassion in addition to high standards of medical care. *"
            },
            {
                header: "Behavioral Therapy",
                icon: "icon-i-mental-health",
                details: "Nowadays a lot of impetus is given on psychological and behavioral well-being. " +
                "Appropriate testings and assessments are made for disorders like ADHD, " +
                "learning disabilities and autism. We provide proper care and management " +
                "for these problems. We provide counsellings, behavior therapies, etc. " +
                "We provide individual counsellings and management for adolescent/teenage " +
                "issues of aggression, drug abuse, smoking, etc."
            },
            {
                header: "Maternal Care",
                icon: "icon-i-womens-health",
                details: "Before your Baby arrives, we help you choose your baby's pediatrician and to shed the " +
                "apprehensions about bringing your child to this world and allaying fears and anxiety associated " +
                "with it. Wee Care Pediatrics offers \"meet the baby doctor\" visits. It will allow you to see " +
                "where our clinic is located and have some first-hand experience with our staff. You should ask " +
                "any questions you have about the birth process, hospital experience and what to expect in first " +
                "few weeks of life. We want you to feel comfortable with your baby's doctor.We highly recommend " +
                "you attend a breastfeeding education visit before the birth of your infant. We want you to " +
                "feel comfortable with your baby's doctor."
            },
            {
                header: "Community",
                icon: "icon-i-social-services",
                details: "Dr. Ashish Gupta is providing school services like lectures and periodic " +
                "health check-ups. He helps schools in establishing their medical rooms. He holds " +
                "free camps at his clinic and in community level like villages, slums, etc. under " +
                "the name of We Care Foundation."
            }
        ];
    });
