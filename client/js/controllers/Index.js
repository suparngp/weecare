/**
 * Created by suparngupta on 1/17/16.
 */
angular.module("weecare")
    .controller("Index", function ($scope, $timeout) {
        $scope.highlightServices = [];
        $scope.otherServices = [
            [{
                header: "Preventive Care",
                icon: "icon-i-immunizations",
                details: "Providing high quality vaccinations as per national and international guidelines. Ask for vaccination calendar on your next visit"
            }, {
                header: "Respiratory Disorders",
                icon: "icon-i-respiratory",
                details: "Specializing in respiratory diseases including wheezing episodes, Asthma, treatment of allergies"
            }],
            [{
                header: "Nutrition",
                icon: "icon-i-nutrition",
                details: "Proper nutrition is utmost important for your new born. Receive expert advise on the matter."
            }, {
                header: "New Born Care",
                icon: "icon-i-pediatrics",
                details: "Attending term and pre-mature deliveries and providing care in NICU*. Regular follow up and holistic care for your baby."
            }],
            [{
                header: "Growth & Development",
                icon: "icon-i-alternative-complementary",
                details: "Periodic health examinations, monitoring metrics like weight which are vital for baby's development"
            }, {
                header: "Intensive / In-bed care",
                icon: "icon-i-inpatient",
                details: "In our state of the art facilities, we treat your child with compassion and highest standards of medical care.*"
            }],
            [{
                header: "Behavioral Therapy",
                icon: "icon-i-mental-health",
                details: "Allow your child to express without consequences. Get trustworthy counselling in overcoming disorders like autism."
            }, {
                header: "Community",
                icon: "icon-i-social-services",
                details: "Serving community via 'We Care' Foundation and organizing free health check up camps at various local institutions"
            }]
        ];

        $scope.gallery = [];

        var indices = [1, 2, 3, 4, 5, 6, 7, 8];
        for(var i in indices){
            $scope.gallery.push({src: "/dist/images/" + indices[i] + ".jpg", srct: "/dist/images/" + indices[i] + ".jpg"});
        }

        $timeout(function(){

            $("#gallery").nanoGallery( {
                thumbnailHoverEffect: "scale120",
                thumbnailWidth: "auto",
                thumbnailHeight: 200,
                thumbnailGutterWidth: 30,
                thumbnailGutterHeight: 30,
                thumbnailLabel: {
                    display: false
                },
                theme: "clean",
                items: $scope.gallery
            });
        }, 400);
    });