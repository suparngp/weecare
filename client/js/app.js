/**
 * Created by suparngupta on 1/16/16.
 */

var app = angular.module("weecare", ["ui.router"]);

app.config(function ($stateProvider, $urlRouterProvider, templateProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('home', {
            url: "/",
            templateUrl: templateProvider.fetchPageWithName("index"),
            controller: "Index",
            label: "Home"
        }).state('about', {
            url: '/about',
            templateUrl: templateProvider.fetchPageWithName("about"),
            controller: "About",
            label: "About"
        })
        .state('services', {
            url: '/services',
            templateUrl: templateProvider.fetchPageWithName("services"),
            controller: 'Services',
            label: "Services"
        });
});